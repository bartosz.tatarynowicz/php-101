<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Tytuł strony</title>

</head>
<body>
	<ul>
		<?php foreach ($tasks as $task) : ?>
			<li>
			<!-- <?= $task->getDescription(); ?> -->
			<?php if ($task->isComplete()) : ?>
				<strike><?= $task->getDescription(); ?></strike>
			<?php else : ?>
				<?= $task->getDescription(); ?>
			<?php endif; ?>
			</li>
		<?php endforeach; ?>
	</ul>
</body>
</html>