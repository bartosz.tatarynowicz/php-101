<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Tytuł strony</title>

</head>
<body>

	<h1>Task For The Day</h1>
	<ul>
		<!-- <?php foreach ($task as $key => $value) : ?>
		<li>
			<strong><?= ucwords($key); ?> :</strong><?= $value; ?> 
		</li>
		<?php endforeach; ?> -->

		<li>
			<strong>Name: </strong> <?= $task['title']; ?>
		</li>
		<li>
			<strong>Due Date: </strong> <?= $task['due']; ?>
		</li>
		<li>
			<strong>Person Responsible: </strong> <?= $task['assigned_to']; ?>
		</li>
		<li>
			<strong>Status: </strong> 
			<!-- <?php

			if ($task['completed'])
			{
				echo '&#9989;';
			}
			else
			{
				echo 'Incomplete';
			}
			?> -->

			<?php if ($task['completed']) : ?>
				<span class="icon">&#9989;</span>
			<?php else : ?>
				<span class="icon">Incomplete</span>
			<?php endif ?>
		</li>



	</ul>

</body>
</html>